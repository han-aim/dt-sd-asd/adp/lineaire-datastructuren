#![deny(clippy::all)]
#![deny(clippy::pedantic)]
#![allow(clippy::needless_return)]

pub mod lijst;
pub mod lijst_rij;
pub mod stapel;
