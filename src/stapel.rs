const GEHEUGEN: usize = 16;

/// Stapel met vaste capaciteit, gebaseerd op een onderliggende statische rij van `GEHEUGEN` lang.
///
/// Deze eenvoudige implementatie geeft een uitzondering (exception) bij onmogelijke operaties, in plaats van een resultaatwaarde die een foutconditie aangeeft.
#[derive(Copy, Clone, Debug)]
pub struct Stapel<Item> {
    /// Bovenste (top) van de stapel.
    index_bovenste: usize,
    /// Onderliggende rij
    rij: [Item; GEHEUGEN],
    leeg: bool,
}

impl<Item: Copy> Stapel<Item> {
    pub fn new(item: Item) -> Self {
        Self {
            leeg: false,
            index_bovenste: 0,
            rij: [item; GEHEUGEN],
        }
    }

    /// Neem bovenste item van stapel af.
    ///
    /// Het item op `index_bovenste` wordt niet overschreven.
    /// Dat is immers onnodig.
    #[must_use]
    #[allow(clippy::missing_panics_doc, clippy::comparison_chain)]
    pub fn pop(&mut self) -> Item {
        if self.leeg {
            panic!("De stapel is leeg. ")
        } else {
            let item = self.rij[self.index_bovenste];
            if self.index_bovenste == 0 {
                self.leeg = true;
            } else if self.index_bovenste > 0 {
                self.index_bovenste -= 1;
            }
            return item;
        }
    }

    /// Leg bovenste item op stapel.
    #[allow(clippy::missing_panics_doc)]
    pub fn push(&mut self, item: Item) {
        let index_laatste = self.rij.len() - 1;
        self.index_bovenste += 1;
        self.leeg = false;
        if self.index_bovenste > index_laatste {
            self.index_bovenste -= 1;
            panic!("De stapel is vol. ");
        } else {
            self.rij[self.index_bovenste] = item;
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn push_1() {
        let mut stapel = Stapel::new(1);
        stapel.push(2);
    }

    #[test]
    fn push_meerdere() {
        const STAPEL: &str = "Stapel { index_bovenste: 15, rij: [123, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], leeg: false }";
        let mut stapel = Stapel::new(123);
        let mut teller = 1;
        while teller < GEHEUGEN {
            stapel.push(teller);
            teller += 1;
        }
        assert_eq!(format!("{stapel:?}", stapel = stapel), STAPEL);
    }

    #[test]
    fn pop_1() {
        let mut stapel = Stapel::new(1);
        assert_eq!(stapel.pop(), 1);
    }

    #[test]
    fn pop_push_push_pop_pop() {
        let mut stapel = Stapel::new(1);
        assert_eq!(stapel.pop(), 1);
        stapel.push(2);
        stapel.push(3);
        assert_eq!(stapel.pop(), 3);
        assert_eq!(stapel.pop(), 2);
    }

    #[test]
    fn pop_meerdere() {
        const STAPEL: &str = "Stapel { index_bovenste: 0, rij: [123, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15], leeg: false }";
        let mut stapel = Stapel::new(123);
        let mut teller = 1;
        while teller < GEHEUGEN {
            stapel.push(teller);
            teller += 1;
        }
        let mut teller = 1;
        while teller < GEHEUGEN {
            let _ = stapel.pop();
            teller += 1;
        }
        assert_eq!(format!("{stapel:?}", stapel = stapel), STAPEL);
    }

    #[should_panic]
    #[test]
    fn pop_excessief() {
        let mut stapel = Stapel::new(1);
        let _ = stapel.pop();
        let _ = stapel.pop();
    }
}
