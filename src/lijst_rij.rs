use super::lijst::Lijst;

const GEHEUGEN: usize = 256;

pub struct LijstRij<Item>
where
    Item: Copy,
{
    index: usize,
    rij: [Option<Item>; GEHEUGEN],
}

impl<Item> Lijst<Item> for LijstRij<Item>
where
    Item: Copy,
{
    /// Remove all contents from the list, so it is once again empty
    fn leeg(&mut self) {
        self.rij = [None; GEHEUGEN];
    }

    /// Vergeet laatste item in de rij, aangezien de rij een vaste grootte heeft.
    fn voeg_in(&mut self, item: Item) {
        let lengte = self.rij.len();
        self.rij.copy_within(self.index..lengte - 1, self.index);
        self.rij[self.index] = Some(item);
        self.index += 1;
    }

    fn voeg_aan(&mut self, item: Item) {
        if self.index < self.rij.len() {
            self.rij[self.index] = Some(item);
            self.index += 1;
        }
    }

    fn verwijder(&mut self) -> Option<Item> {
        let waarde = self.krijg_waarde();
        if waarde.is_some() {
            self.rij[self.index] = None;
            self.index -= 1;
        }
        return waarde;
    }

    fn stelin_index_start(&mut self) {
        self.index = 0;
    }

    fn stelin_index_einde(&mut self) {
        self.index = self.lengte() + 1;
    }

    fn vorige(&mut self) {
        if self.index > 0 {
            self.index -= 1;
        }
    }

    fn volgende(&mut self) {
        if !self.is_aan_einde() {
            self.index += 1;
        }
    }

    fn lengte(&mut self) -> usize {
        self.stelin_index_start();
        let mut teller = 0;
        while self.krijg_waarde().is_some() {
            teller += 1;
            self.volgende();
        }
        return teller;
    }

    fn krijg_index(&self) -> usize {
        return self.index;
    }

    fn stelin_index(&mut self, index: usize) {
        return self.index = index;
    }

    fn is_aan_einde(&mut self) -> bool {
        return self.index == self.lengte() - 1;
    }

    fn krijg_waarde(&self) -> Option<Item> {
        return self.rij[self.index];
    }

    fn is_leeg(&mut self) -> bool {
        self.lengte() == 0
    }
}
